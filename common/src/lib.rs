pub const MAX_NAME_LENGTH: usize = 20;
pub const GAMESERVER_PUBLISH_TIMEOUT: std::time::Duration = std::time::Duration::from_secs(6);
